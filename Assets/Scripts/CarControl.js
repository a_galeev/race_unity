﻿#pragma strict
var wheelFL : WheelCollider;
var wheelFR : WheelCollider;
var wheelBL : WheelCollider;
var wheelBR : WheelCollider;
var wheelFLTrans : Transform;
var wheelFRTrans : Transform;
var wheelBLTrans : Transform;
var wheelBRTrans : Transform;
var lowestSteerAtSpeed : float = 50;
var lowSpeedSteerAngel : float = 10;
var highSpeedSteerAngel : float = 1;
var decelerationSpeed : float = 30;
var maxTorque : float  =50;
var curSpeed : float;
var topSpeed: float = 150;
var maxRevSpeed: float = 5;
var backLightObj : GameObject;
var idleLightMat: Material;
var revLightMat : Material;
var brakeLightMat : Material;
private var braked :boolean = false;
var maxBrakeTorque = 100;
private var mySidewayFriction :float;
private var myForwardFriction :float;
private var slipSidewayFriction :float;
private var slipForwardFriction :float;
var speedometerDial : Texture2D;
var speedometerPointer : Texture2D;
var spark:GameObject;
var collisionSound:GameObject;
var gearRatio:int[] = [20,50,80,110,160];

function Start()
{
	GetComponent.<Rigidbody>().centerOfMass.y = -0.5;
	GetComponent.<Rigidbody>().centerOfMass.z = 0.5;
	SetValues();
}

function SetValues()
{
	myForwardFriction = wheelBR.forwardFriction.stiffness;
	mySidewayFriction = wheelBR.sidewaysFriction.stiffness;
	slipSidewayFriction = 0.085;
	slipForwardFriction = 0.05;
}

function FixedUpdate () 
{
    Controle ();
   	HandBrake();

}
function Update()
{
    wheelFLTrans.Rotate(wheelFL.rpm/60*360*Time.deltaTime,0,0);
    wheelFRTrans.Rotate(wheelFR.rpm/60*360*Time.deltaTime,0,0);
    wheelBLTrans.Rotate(wheelBL.rpm/60*360*Time.deltaTime,0,0);
    wheelBRTrans.Rotate(wheelBR.rpm/-60*360*Time.deltaTime,0,0);
    wheelFLTrans.localEulerAngles.y = wheelFL.steerAngle - wheelFLTrans.localEulerAngles.z;
    wheelFRTrans.localEulerAngles.y = wheelFR.steerAngle - wheelFRTrans.localEulerAngles.z;
	BackLight();
	WheelPos();
	EngineSound();
}
function Controle ()
{
	//curSpeed = Mathf.RoundToInt(2*Mathf.PI*wheelFL.radius*wheelFL.rpm*6/100);
	curSpeed = Mathf.Round(GetComponent.<Rigidbody>().velocity.magnitude * 3.6);

	if (curSpeed < topSpeed && curSpeed > -maxRevSpeed && !braked)
		{
			wheelBR.motorTorque = maxTorque * Input.GetAxis("Vertical");
			wheelBL.motorTorque = maxTorque * Input.GetAxis("Vertical");
		}
	else 
		{
			wheelBR.motorTorque =0;
			wheelBL.motorTorque =0;
		}
	if (Input.GetButton("Vertical")==false)
		{
			wheelBR.brakeTorque = decelerationSpeed;
			wheelBL.brakeTorque = decelerationSpeed;
		}
	else
		{
			wheelBR.brakeTorque = 0;
			wheelBL.brakeTorque = 0;
		}
	var speedFactor = GetComponent.<Rigidbody>().velocity.magnitude/lowestSteerAtSpeed;
	var currentSteerAngel = Mathf.Lerp(lowSpeedSteerAngel,highSpeedSteerAngel,speedFactor);
	currentSteerAngel *= Input.GetAxis("Horizontal");
	wheelFL.steerAngle = currentSteerAngel;
	wheelFR.steerAngle = currentSteerAngel;
}
function BackLight()
{
	if(curSpeed > 0 && Input.GetAxis("Vertical") <0 && !braked)
		{
			backLightObj.GetComponent.<Renderer>().material = brakeLightMat;
		}
	else if	(curSpeed < 0 && Input.GetAxis("Vertical") >0 && !braked)
		{
			backLightObj.GetComponent.<Renderer>().material = brakeLightMat;
		}
	else if	(curSpeed < 0 && Input.GetAxis("Vertical") <0 && !braked)
		{
			backLightObj.GetComponent.<Renderer>().material = revLightMat;
		}	
	else if (!braked)
		{
			backLightObj.GetComponent.<Renderer>().material = idleLightMat;	
		}
}	
function WheelPos()
{
	var hit: RaycastHit;
	var wheelPos: Vector3;
	if(Physics.Raycast(wheelFL.transform.position, -wheelFL.transform.up,hit ,wheelFL.radius+wheelFL.suspensionDistance))
		{
			wheelPos = hit.point+wheelFL.transform.up*wheelFL.radius;
		}
	else
		{
			wheelPos = wheelFL.transform.position - wheelFL.transform.up * wheelFL.suspensionDistance;
		}
	wheelFLTrans.position = wheelPos;
	
	if(Physics.Raycast(wheelFR.transform.position, -wheelFR.transform.up,hit ,wheelFR.radius+wheelFR.suspensionDistance))
		{
			wheelPos = hit.point+wheelFR.transform.up*wheelFR.radius;
		}
	else
		{
			wheelPos = wheelFR.transform.position - wheelFR.transform.up * wheelFR.suspensionDistance;
		}
	wheelFRTrans.position = wheelPos;
	
	if(Physics.Raycast(wheelBL.transform.position, -wheelBL.transform.up,hit ,wheelBL.radius+wheelBL.suspensionDistance))
		{
			wheelPos = hit.point+wheelBL.transform.up*wheelBL.radius;
		}
	else
		{
			wheelPos = wheelBL.transform.position - wheelBL.transform.up * wheelBL.suspensionDistance;
		}
	wheelBLTrans.position = wheelPos;
	
	if(Physics.Raycast(wheelBR.transform.position, -wheelBR.transform.up,hit ,wheelBR.radius+wheelBR.suspensionDistance))
		{
			wheelPos = hit.point+wheelBR.transform.up*wheelBR.radius;
		}
	else
		{
			wheelPos = wheelBR.transform.position - wheelBR.transform.up * wheelBR.suspensionDistance;
		}
	wheelBRTrans.position = wheelPos;
}

function HandBrake()
{
	if (Input.GetButton("Jump"))
		{
			braked = true;
		}
	else
		{
			braked = false;
		}
	if (braked)
		{
			if (curSpeed > 1)
				{
					wheelFR.brakeTorque = maxBrakeTorque;
					wheelFL.brakeTorque = maxBrakeTorque;
					wheelBR.motorTorque =0;
					wheelBL.motorTorque =0;
					SetRearSlip(slipForwardFriction ,slipSidewayFriction);
				}
			else if (curSpeed < 0)
				{
					wheelBR.brakeTorque = maxBrakeTorque;
					wheelBL.brakeTorque = maxBrakeTorque;
					wheelBR.motorTorque =0;
					wheelBL.motorTorque =0;
					SetRearSlip(1 ,1);
				}
			else 
				{
					SetRearSlip(1 ,1);
				}
			if (curSpeed < 1 && curSpeed > -1)
				{
					backLightObj.GetComponent.<Renderer>().material = idleLightMat;
				}
			else 
				{
					backLightObj.GetComponent.<Renderer>().material = brakeLightMat;
				}
	}
	else 
		{
			wheelFR.brakeTorque = 0;
			wheelFL.brakeTorque = 0;
			SetRearSlip(myForwardFriction ,mySidewayFriction);
		}
	}
function ReverseSlip()
{
	if (curSpeed <0)
		{
			SetFrontSlip(slipForwardFriction ,slipSidewayFriction);
		}
	else 
		{
			SetFrontSlip(myForwardFriction ,mySidewayFriction);
		}
}
 
function SetRearSlip (currentForwardFriction : float,currentSidewayFriction : float)
{
	wheelBR.forwardFriction.stiffness = currentForwardFriction;
	wheelBL.forwardFriction.stiffness = currentForwardFriction;
	wheelBR.sidewaysFriction.stiffness = currentSidewayFriction;
	wheelBL.sidewaysFriction.stiffness = currentSidewayFriction;
}
function SetFrontSlip (currentForwardFriction : float,currentSidewayFriction : float)
{
	wheelFR.forwardFriction.stiffness = currentForwardFriction;
	wheelFL.forwardFriction.stiffness = currentForwardFriction;
	wheelFR.sidewaysFriction.stiffness = currentSidewayFriction;
	wheelFL.sidewaysFriction.stiffness = currentSidewayFriction;
}
/*function SetSlip(curForFriction:float, curSideFriction:float)
{
	wheelBR.forwardFriction.stiffness = curForFriction;
	wheelBL.forwardFriction.stiffness = curForFriction;
	//wheelFR.forwardFriction.stiffness = curForFriction;
	//wheelFL.forwardFriction.stiffness = curForFriction;
	
	wheelBR.sidewaysFriction.stiffness = curSideFriction;
	wheelBL.sidewaysFriction.stiffness = curSideFriction;
	//wheelFR.sidewaysFriction.stiffness = curSideFriction;
	//wheelFL.sidewaysFriction.stiffness = curSideFriction;
}*/

function EngineSound()
{
	for(var i=0;i<gearRatio.Length;i++)
		{
			if(gearRatio[i]>curSpeed)
				{
					break;
				}
		}
	var gearMinValue:float = 0.00;
	var gearMaxValue:float = 0.00;
	if (i == 0)
		{
			gearMinValue = 0;
			
		}
	else
		{
			gearMinValue = gearRatio[i-1];
		}
	gearMaxValue = gearRatio[i];
	var enginePitch : float = ((curSpeed - gearMinValue)/(gearMaxValue - gearMinValue))+1;
	//GetComponent.<AudioSource>().pitch = curSpeed/topSpeed+1;
	GetComponent.<AudioSource>().pitch = enginePitch;
}
function OnGUI()
{
	GUI.DrawTexture(Rect(Screen.width - 300, Screen.height - 150, 300, 150), speedometerDial);
	var speedFactor:float = curSpeed / topSpeed;
	var rotationAngle :float;
	if(curSpeed >=0)
		{
			rotationAngle = Mathf.Lerp(0,180,speedFactor);
		}
	else 
		{
			rotationAngle = Mathf.Lerp(0,180,-speedFactor);
		}
	GUIUtility.RotateAroundPivot(rotationAngle, Vector2(Screen.width-150, Screen.height));
	GUI.DrawTexture(Rect(Screen.width - 300, Screen.height - 150, 300, 300), speedometerPointer);
}

function OnCollisionEnter(other : Collision)
{
	if(other.transform != transform && other.contacts.Length !=0)
		{
			for (var i =0; i<other.contacts.Length; i++)
				{
					Instantiate(spark, other.contacts[i].point, Quaternion.identity);
					Instantiate(collisionSound, other.contacts[i].point, Quaternion.identity);
				}
		}
	
}