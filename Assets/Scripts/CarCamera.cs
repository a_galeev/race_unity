﻿using UnityEngine;
using System.Collections;

public class CarCamera : MonoBehaviour
{
    public Transform car;
    public float distance = 6.4f;
    public float height = 1.4f;
    public float rotDamping = 3.0f;
    public float heightDamping = 2.0f;
    public float zoomRatio = 0.5f;
    public float defaultFOV = 60f;
    Vector3 rotVec;
    // Use this for initialization

    void FixedUpdate()
    {

        Vector3 localVelocity = car.InverseTransformDirection(car.GetComponent<Rigidbody>().velocity);
        if (localVelocity.z < -0.5f)
        {
            //Vector3 temp = rotVec; //because temporary variables seem to be removed after a closing bracket "}" we can use the same variable name multiple times.
            //temp.y = car.eulerAngles.y + 180;
            //rotVec = temp;
            rotVec.y = car.eulerAngles.y + 180.0f;
        }
        else
        {
            //Vector3 temp = rotVec;
            //temp.y = car.eulerAngles.y;
            //rotVec = temp;
            rotVec.y = car.eulerAngles.y;
        }
        float acc = car.GetComponent<Rigidbody>().velocity.magnitude;



        GetComponent<Camera>().fieldOfView = defaultFOV + acc * zoomRatio * Time.deltaTime;  //he removed * Time.deltaTime but it works better if you leave it like this.

    }

    // Update is called once per frame
    void LateUpdate()
    {
        float wantedAngle = car.eulerAngles.y;
        float wantedHeight = car.position.y + height;
        float myAngle = transform.eulerAngles.y;
        float myHeight = transform.position.y;

        myAngle = Mathf.LerpAngle(myAngle, wantedAngle, rotDamping * Time.deltaTime);
        myHeight = Mathf.Lerp(myHeight, wantedHeight, heightDamping * Time.deltaTime);

        Quaternion currentRotation = Quaternion.Euler(0, myAngle, 0);
        transform.position = car.position;
        transform.position -= currentRotation * Vector3.forward * distance;
        //Vector3 temp = transform.position; //temporary variable so Unity doesn't complain
        //temp.y = myHeight;
        //transform.position = temp;
        transform.position = new Vector3(transform.position.x, myHeight, transform.position.z);
        transform.LookAt(car);

    }
}
