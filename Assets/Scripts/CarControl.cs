﻿/*using UnityEngine;
using System.Collections;


public class CarControl : MonoBehaviour
{
    public WheelCollider WheelFL;
    public WheelCollider WheelFR;
    public WheelCollider WheelBL;
    public WheelCollider WheelBR;
    public Transform WheelFLT;
    public Transform WheelFRT;
    public Transform WheelBLT;
    public Transform WheelBRT;
    public GameObject backLightObj;
    public Material idleLightMat;
    public Material brakeLightMat;
    public Material reverseLightMat;
    public float curSpeed;
    RaycastHit hit;
    float MaxTorque = 500;
    
    float topSpeed = 88;
    float maxReverseSpeed = 20;
    float lowestSteerAtSpeed = 50;
    float decSpeed = 100;
    float lsSteerAngel = 10;
    float hsSteerAngel = 1;
    float Angel = 10;


    Vector3 temp, temp1, temp2;
    float pi = 3.14f;

    // Use this for initialization
    void Start()
    {
        temp = GetComponent<Rigidbody>().centerOfMass;
        temp.y = -0.7f; 
        GetComponent<Rigidbody>().centerOfMass = temp;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Controle();   
    }
    void Update()
    {
        WheelFLT.Rotate(WheelFL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        WheelFRT.Rotate(WheelFR.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        WheelBLT.Rotate(WheelBL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        WheelBRT.Rotate(WheelBR.rpm / -60 * 360 * Time.deltaTime, 0, 0);
        temp1 = WheelFLT.localEulerAngles;
        temp2 = WheelFRT.localEulerAngles;
        temp1.y = WheelFL.steerAngle - WheelFLT.localEulerAngles.z;
        temp2.y = WheelFR.steerAngle - WheelFRT.localEulerAngles.z;
        WheelFLT.localEulerAngles = temp1;
        WheelFRT.localEulerAngles = temp2;
        BackLight();
        WheelPosition();
    }

    void Controle()
    {
        curSpeed = Mathf.RoundToInt(2 * pi * WheelBL.radius * WheelBL.rpm * 0.06f);

        //curSpeed = Mathf.RoundToInt(GetComponent<Rigidbody>().velocity.magnitude * 3.6f);
        if (curSpeed <= topSpeed && curSpeed > -maxReverseSpeed)
        {
            WheelBR.motorTorque = MaxTorque * Input.GetAxis("Vertical");
            WheelBL.motorTorque = MaxTorque * Input.GetAxis("Vertical");
        }
        else
        {
            WheelBR.motorTorque = 0;
            WheelBL.motorTorque = 0;
        }
        if (Input.GetButton("Vertical") == false)
        {
            WheelBR.brakeTorque = decSpeed;
            WheelBL.brakeTorque = decSpeed;
        }
        else
        {
            WheelBL.brakeTorque = 0;
            WheelBR.brakeTorque = 0;
        }
        WheelFR.steerAngle = Angel * Input.GetAxis("Horizontal");
        WheelFL.steerAngle = Angel * Input.GetAxis("Horizontal");
        float speedFactor = GetComponent<Rigidbody>().velocity.magnitude / lowestSteerAtSpeed;
        float currentSteerAngel = Mathf.Lerp(lsSteerAngel, hsSteerAngel, speedFactor);
        currentSteerAngel *= Input.GetAxis("Horizontal");
        WheelFL.steerAngle = currentSteerAngel;
        WheelFR.steerAngle = currentSteerAngel;
    }
    void BackLight()
    {
        if(curSpeed> 0 && Input.GetAxis("Vertical")<0)
        {
            backLightObj.GetComponent<Renderer>().material = brakeLightMat;
        }
        else if(curSpeed < 0 && Input.GetAxis("Vertical") > 0)
        {
            backLightObj.GetComponent<Renderer>().material = reverseLightMat;
        }
        else backLightObj.GetComponent<Renderer>().material = idleLightMat;

    }
    void WheelPosition()
    {
        

        if(Physics.Raycast(WheelFL.transform.position, -WheelFL.transform.up, WheelFL.radius+WheelFL.suspensionDistance,hit))
        {

        }
    }
}
*/